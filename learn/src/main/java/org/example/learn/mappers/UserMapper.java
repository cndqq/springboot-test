package org.example.learn.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.example.learn.beans.User;

@Mapper
public interface UserMapper {
    /**
     *
     *注解@Param("userId") userId 对应 #{userId}
     *
     * @param userId
     * @return
     */
    @Select("SELECT user_id userId ,user_name userName from user where user_id = #{userId}")
    User getUserById(@Param("userId") String userId);

    /**
     * #{userName}
     * 取值符号 #{}
     * userName 就是 user中的userName
     * @param user
     * @return
     */
    @Update("update user set user_name = #{userName} where user_id = #{userId}")
    int updateUser(User user);
}
