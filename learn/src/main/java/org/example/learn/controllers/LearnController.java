package org.example.learn.controllers;

import org.example.learn.beans.User;
import org.example.learn.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

// 路由
@RestController
// 类路径过滤
@RequestMapping("/learn")
public class LearnController {
    @Autowired
    UserMapper userMapper;

    // 定义一个post 请求
    @PostMapping  ("/getUser")
    public User getUser(@RequestBody User user){
        return user;
    }

    @GetMapping   ("/getUserById")
    public User getUserId(@RequestParam String id){
        /*User user = new User();
        user.setUserId(id);
        user.setUserName("szm");*/
        User user = userMapper.getUserById(id);
        return user;
    }

    @PostMapping  ("/updateUser")
    public User updateUser(@RequestBody User user){
        int i = userMapper.updateUser(user);

        User userById = userMapper.getUserById(user.getUserId());
        return userById;
    }


}
